﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject[] targetObject;
	private float distanceToTarget;
	public float targetObjectX;
	// Use this for initialization
	void Start () {

		distanceToTarget = transform.position.x - targetObject[0].transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		targetObjectX = targetObject[0].transform.position.x;
		Vector3 newCameraPosition = transform.position;
		newCameraPosition.x = targetObjectX + distanceToTarget;
		transform.position = newCameraPosition;
	}
}
