﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {

	//1
	public Sprite laserOnSprite;    
	public Sprite laserOffSprite;
	public float lowerBound = 20.5f;
	public float incrementRatio;
	public int coef;
	//2    
	public float interval = 2f;      
	public float rotationSpeed;
	
	//3
	private bool isLaserOn = true;    
	private float timeUntilNextToggle;
	public int level = GeneratorScript.currentLevel;
	// Use this for initialization
	void Start () {
		timeUntilNextToggle = interval;

	}
	
	void FixedUpdate () {
		//1
		timeUntilNextToggle -= Time.fixedDeltaTime;
		//2
		rotationSpeed = GeneratorScript.laserRotationSpeed;
		if (timeUntilNextToggle <= 0) {
			
			//3
			isLaserOn = !isLaserOn;
			
			//4
			collider2D.enabled = isLaserOn;
			
			//5
			SpriteRenderer spriteRenderer = ((SpriteRenderer)this.renderer);
			if (isLaserOn)
			{
				spriteRenderer.sprite = laserOnSprite;
				interval = 2f;
			}
			else
			{
				spriteRenderer.sprite = laserOffSprite;
				interval = 1f;
			}
			
			//6
			timeUntilNextToggle = interval;
		}


		//7
		transform.RotateAround(transform.position, Vector3.forward, rotationSpeed * Time. fixedDeltaTime);
	}
}
