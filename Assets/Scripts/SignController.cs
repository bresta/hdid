﻿using UnityEngine;
using System.Collections;

public class SignController : MonoBehaviour {
	public float moveSpeed;
	public int timeExist;
	GameObject saw;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		this.transform.Translate (moveSpeed, 0, 0);
		timeExist--;
		if (timeExist <= 0)
			Destroy (this.gameObject);
	}
}
