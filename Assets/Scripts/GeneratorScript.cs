﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratorScript : MonoBehaviour {
	public GameObject[] availableRooms;
	
	public List<GameObject> currentRooms;
	public int coef;
	private float screenWidthInPoints;

	public List<GameObject> availableObjects;
	public GameObject[] availableCoins;
	public GameObject[] availableObstacles;

	int sawRatio = 1;
	int laserRatio = 1;
	public static int currentLevel;
	public int edge = 0;
	public int upperBound = 2;
	public int lowerBound = 1;
	public int laserEdge = 0;
	public int laserUpperBound = 2;
	public int laserLowerBound = 1;
	public static float laserRotationSpeed = 20.5f;
	public Texture2D levelIconTexture;

	private bool lowerIsOdd;
	public uint currentCoins;
	uint lastCheckpoint;
	private int coeffSaw;
	public GameObject sign;
	public List<GameObject> objects;
	public List<GameObject> preAvailableObjects;
	public float objectsMinDistance = 5.0f;    
	public float objectsMaxDistance = 10.0f;
	
	public float objectsMinY = -1.4f;
	public float objectsMaxY = 1.4f;
	
	public float objectsMinRotation = -45.0f;
	public float objectsMaxRotation = 45.0f;
	public float objectMoveSpeed;
	private float screenWidth;
	// Use this for initialization
	void Start () {
		availableObjects.Clear ();
		float height = 2.0f * Camera.main.orthographicSize;
		screenWidthInPoints = height * Camera.main.aspect;
		screenWidth = (Camera.main.orthographicSize * 2.0f) * Camera.main.aspect;
		lowerIsOdd = false;
		availableObjects = new List<GameObject>();
		UpdateAvailableObject();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate () 
	{    
		GenerateRoomIfRequired();
		GenerateObjectsIfRequired();
		UpdateLevel ();
	}

	public void UpdateRotationSpeed(){
		coef = (currentLevel - 1) / 10;
		laserRotationSpeed = 20.5f + ((float)((currentLevel * 0.5f) - (coef * 2.5f)));
	}

	void UpdateLaserRatio()
	{
		if (currentLevel % 5 == 0) 
		{
			laserEdge++;
			if (laserEdge == 3)
			{
				laserEdge = 0;
				laserLowerBound++;
				laserUpperBound++;
			}
		}
		
		if((laserEdge == 0 || laserEdge == 2))
		{
			laserRatio = laserLowerBound;
		} else if (laserEdge==1)
		{
			laserRatio = laserUpperBound;
		}
	}

	void UpdateSawRatio()
	{
		if (currentLevel % 10 == 0) 
		{
			edge++;
			if (edge == 3)
			{
				edge = 0;
				lowerBound++;
				upperBound++;
			}
		}

		if((edge == 0 || edge == 2))
		{
			sawRatio = lowerBound;
		} else if (edge==1)
		{
			sawRatio = upperBound;
		}


	}


	void UpdateLevel()
	{
		currentCoins = MouseController.coins;
		
		if (currentCoins >= lastCheckpoint + 10) 
		{
			lastCheckpoint = currentCoins;
			currentLevel = currentLevel + 1;
			UpdateSawRatio ();
			UpdateLaserRatio();
			UpdateAvailableObject();
			UpdateRotationSpeed();
		}
	}

	void UpdateAvailableObject(){
		availableObjects.Clear ();
		for (int i=0; i<5; i++) {
			//availableObjects.Add( (GameObject)Instantiate(preAvailableObjects[i]));
			//Destroy(availableObjects[i], 1.0f);
			availableObjects.Add( preAvailableObjects[i]);
		}
		for (int i=0; i<sawRatio; i++) {
			//availableObjects.Add((GameObject)Instantiate(preAvailableObjects[5]));	
			availableObjects.Add(preAvailableObjects[5]);		
		}
		for (int i=0; i<laserRatio; i++) {
			//availableObjects.Add((GameObject)Instantiate(preAvailableObjects[6]));		
			availableObjects.Add(preAvailableObjects[6]);		
		}
		if (currentLevel % 5 == 0) {
			availableObjects.Add(preAvailableObjects[7]);		
		}
		//Debug.Log (availableObjects.Count());
	}

	void OnGUI()
	{
		DisplayLevel();
	}

	void DisplayLevel()
	{                       

		GUIStyle style = new GUIStyle();
		style.fontSize = 30;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		Rect levelRect = new Rect(320, 10, 32, 32);
		GUI.DrawTexture(levelRect, levelIconTexture); 
		GUI.Label(new Rect(360, 10, 32, 32), currentLevel.ToString(), style);
	}

	void AddRoom(float farhtestRoomEndX)
	{
		//1
		int randomRoomIndex = Random.Range(0, availableRooms.Length);
		
		//2
		GameObject room = (GameObject)Instantiate(availableRooms[randomRoomIndex]);
		
		//3
		float roomWidth = room.transform.FindChild("floor").localScale.x;
		
		//4
		float roomCenter = farhtestRoomEndX + roomWidth * 0.5f;
		
		//5
		room.transform.position = new Vector3(roomCenter, 0, 0);
		
		//6
		currentRooms.Add(room);         
	}

	void GenerateRoomIfRequired()
	{
		//1
		List<GameObject> roomsToRemove = new List<GameObject>();

		//2
		bool addRooms = true;        
		
		//3
		float playerX = transform.position.x;
		
		//4
		float removeRoomX = playerX - screenWidthInPoints;        
		
		//5
		float addRoomX = playerX + screenWidthInPoints;
		
		//6
		float farthestRoomEndX = 0;
		
		foreach(var room in currentRooms)
		{
			//7
			float roomWidth = room.transform.FindChild("floor").localScale.x;
			float roomStartX = room.transform.position.x - (roomWidth * 0.5f);    
			float roomEndX = roomStartX + roomWidth;                            
			
			//8
			if (roomStartX > addRoomX)
				addRooms = false;
			
			//9
			if (roomEndX < removeRoomX)
				roomsToRemove.Add(room);
			
			//10
			farthestRoomEndX = Mathf.Max(farthestRoomEndX, roomEndX);
		}
		
		//11
		foreach(var room in roomsToRemove)
		{
			currentRooms.Remove(room);
			Destroy(room);            
		}
		
		//12
		if (addRooms)
			AddRoom(farthestRoomEndX);
	}

	void AddObject(float lastObjectX)
	{
		//1
		int randomIndex = Random.Range(0, availableObjects.Count);
		//int randomIndex = Random.Range(0, availableCoins.Length);
		float playerX = transform.position.x;
		float addObjectX = playerX + screenWidthInPoints;
		//2
		GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);
		//GameObject obj = (GameObject)Instantiate(availableCoins[randomIndex]);


		//print (randomIndex);

		//3
		float objectPositionX = lastObjectX + Random.Range(objectsMinDistance, objectsMaxDistance);
		float randomY = Random.Range(objectsMinY, objectsMaxY);
		obj.transform.position = new Vector3(objectPositionX,randomY,0); 
		
		//4
		float rotation = Random.Range(objectsMinRotation, objectsMaxRotation);
		if (!obj.CompareTag("Saws")) obj.transform.rotation = Quaternion.Euler(Vector3.forward * rotation);
		if (obj.CompareTag("Saws")){
			GameObject signObject = (GameObject)Instantiate(sign);
			signObject.transform.position = new Vector3(addObjectX - 2.5f, obj.transform.position.y, 0);
		}
		//5
		objects.Add (obj);
	}

	void AddCoin(float lastCoinX)
	{
		//1
		int randomIndex = Random.Range(0, availableCoins.Length);
		float playerX = transform.position.x;
		float addObjectX = playerX + screenWidthInPoints;
		//2
		GameObject obj = (GameObject)Instantiate(availableCoins[randomIndex]);
		//print (randomIndex);
		
		//3
		float objectPositionX = lastCoinX + Random.Range(objectsMinDistance, objectsMaxDistance);
		float randomY = Random.Range(objectsMinY, objectsMaxY);
		obj.transform.position = new Vector3(objectPositionX,randomY,0); 
		
		//4
		float rotation = Random.Range(objectsMinRotation, objectsMaxRotation);
		obj.transform.rotation = Quaternion.Euler(Vector3.forward * rotation);

		//5
		objects.Add (obj);

	}

	void AddObstacle(float lastObstacleX)
	{

		//1
		int randomIndex = Random.Range(0, availableObstacles.Length);
		float playerX = transform.position.x;
		float addObjectX = playerX + screenWidthInPoints;
		//2
		GameObject obj = (GameObject)Instantiate(availableObstacles[randomIndex]);
		//print (randomIndex);
		
		//3
		float objectPositionX = lastObstacleX + Random.Range(objectsMinDistance, objectsMaxDistance);
		float randomY = Random.Range(objectsMinY, objectsMaxY);
		obj.transform.position = new Vector3(objectPositionX,randomY,0); 
		
		//4
		float rotation = Random.Range(objectsMinRotation, objectsMaxRotation);

		if (obj.CompareTag("Saws")){
			GameObject signObject = (GameObject)Instantiate(sign);
			signObject.transform.position = new Vector3(addObjectX - 1, obj.transform.position.y, 0);
		}
		//5
		objects.Add (obj);
	}

	void GenerateObjectsIfRequired()
	{
		//1
		float playerX = transform.position.x;        
		float removeObjectsX = playerX - screenWidthInPoints;
		float addObjectX = playerX + screenWidthInPoints;
		float farthestObjectX = 0;
		
		//2
		List<GameObject> objectsToRemove = new List<GameObject>();
		
		foreach (var obj in objects)
		{
			//3
			float objX = obj.transform.position.x;
			
			//4
			farthestObjectX = Mathf.Max(farthestObjectX, objX);
			
			//5
			if (objX < removeObjectsX)
				objectsToRemove.Add(obj);
		}
		
		//6
		foreach (var obj in objectsToRemove)
		{
			objects.Remove(obj);
			Destroy(obj);
		}
		
		//7
		if (farthestObjectX < addObjectX)
			AddObject(farthestObjectX);
	}

//	void GenerateCoinsIfRequired()
//	{
//		//1
//		float playerX = transform.position.x;        
//		float removeCoinsX = playerX - screenWidthInPoints;
//		float addCoinsX = playerX + screenWidthInPoints;
//		float farthestCoinX = 0;
//		
//		//2
//		List<GameObject> coinsToRemove = new List<GameObject>();
//		
//		foreach (var obj in objects)
//		{
//			if(obj.CompareTag("single_coin") || obj.CompareTag("coin_M 1") || obj.CompareTag("coin_pyramid") || obj.CompareTag("coin_pyramid2") || obj.CompareTag("coin_square"))
//			{
//					//3
//					float objX = obj.transform.position.x;
//					
//					//4
//					farthestCoinX = Mathf.Max(farthestCoinX, objX);
//					
//					//5
//					if (objX < removeCoinsX)
//						coinsToRemove.Add(obj);
//			}
//		}
//		
//		//6
//		foreach (var obj in coinsToRemove)
//		{
//
//				objects.Remove(obj);
//				Destroy(obj);
//
//		}
//		
//		//7
//		if (farthestCoinX < addCoinsX)
//			AddObject(farthestCoinX);
//	}
}
