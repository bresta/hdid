﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour {
	public float jetpackForce = 75.0f;
	public float forwardMovementSpeed = 3.0f;
	public Transform groundCheckTransform;
	
	private bool grounded;
	public bool isPowered = false;
	public LayerMask groundCheckLayerMask;
	public float PUTimer = 7;
	public ParticleSystem jetpack;
	public ParticleSystem jetpackUp;

	Animator animator;

	private bool dead = false;

	public static uint coins = 0;

	public Texture2D coinIconTexture;

	public AudioClip coinCollectSound;
	public AudioClip chainSawSFX;
	public AudioClip PUSFX;

	public AudioSource jetpackAudio;
	
	public AudioSource footstepsAudio;

	// Use this for initialization
	void Start () {

		animator = GetComponent<Animator>();
		Debug.Log("start");
		GameObject player = GameObject.Find ("player");
		player.particleEmitter.enabled = false;
		jetpack.enableEmission = false;
		jetpackUp.enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void UpdateGroundedStatus()
	{
		//1
		grounded = Physics2D.OverlapCircle(groundCheckTransform.position, 0.1f, groundCheckLayerMask);
		
		//2
		animator.SetBool("grounded", grounded);
	}
	void FixedUpdate () 
	{
		bool jetpackActive = Input.GetButton("Fire1") || Input.GetKey(KeyCode.Space);
		CountDown ();
		jetpackActive = jetpackActive && !dead;
		
		if (jetpackActive)
		{
			rigidbody2D.AddForce(new Vector2(0, jetpackForce));
		}
		
		if (!dead)
		{
			Vector2 newVelocity = rigidbody2D.velocity;
			newVelocity.x = forwardMovementSpeed;
			rigidbody2D.velocity = newVelocity;
		}
		Cheat ();
		UpdateGroundedStatus();
		
		AdjustJetpack(jetpackActive);
		AdjustFootstepsAndJetpackSound(jetpackActive);
	}

	void CountDown(){
		PUTimer -= Time.deltaTime;
		if (PUTimer <= 0) {
			isPowered = false;
			jetpack.enableEmission = false;
			jetpackUp.enableEmission = false;
			//particle dieded
		}
	}

	void Cheat(){
		if (Input.GetKeyDown (KeyCode.F1))
						coins += 10;
	}

	void AdjustJetpack (bool jetpackActive)
	{
		//jetpack.enableEmission = !grounded;
		//jetpackUp.enableEmission = !grounded;
		//jetpack.emissionRate = jetpackActive ? 300.0f : 75.0f; 
		//jetpackUp.emissionRate = jetpackActive ? 300.0f : 75.0f;
		GameObject player = GameObject.Find ("player");
		if (jetpackActive) {
			player.particleEmitter.enabled = true;			
		} else {
			player.particleEmitter.enabled = false;
		}
	}

	void HitByLaser(Collider2D laserCollider)
	{
		if (!dead)
			laserCollider.gameObject.audio.Play();
		dead = true;
		animator.SetBool("dead", true);

	}

	void CollectCoin(Collider2D coinCollider)
	{
		coins++;
		
		Destroy(coinCollider.gameObject);

		AudioSource.PlayClipAtPoint(coinCollectSound, transform.position);
	}

	void CollectPU(Collider2D PUCollider)
	{
		isPowered = true;
		jetpack.enableEmission = true;
		jetpackUp.enableEmission = true;
		//particle aktif
		PUTimer = 7f;
		Destroy(PUCollider.gameObject);
		
		AudioSource.PlayClipAtPoint(PUSFX, transform.position);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag ("Coins"))
						CollectCoin (collider);
		else if (collider.gameObject.CompareTag ("Powerups")){
			CollectPU(collider);
		}
		else if (collider.gameObject.CompareTag ("Saws")) {
			if(!isPowered)
			HitBySaw (collider);

				}
		else if(!isPowered) HitByLaser(collider);
	}

	void HitBySaw(Collider2D saw)
	{
		AudioSource.PlayClipAtPoint (chainSawSFX, transform.position);
		//if (!dead)
		//	saw.gameObject.audio.Play();
		dead = true;
		animator.SetBool("dead", true);
	}

	void DisplayCoinsCount()
	{
		Rect coinIconRect = new Rect(10, 10, 32, 32);
		GUI.DrawTexture(coinIconRect, coinIconTexture);                         
		
		GUIStyle style = new GUIStyle();
		style.fontSize = 30;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.yellow;
		
		Rect labelRect = new Rect(coinIconRect.xMax, coinIconRect.y, 60, 32);
		GUI.Label(labelRect, coins.ToString(), style);
	}

	void OnGUI()
	{
		DisplayCoinsCount();
		DisplayRestartButton();
	}

	void DisplayRestartButton()
	{
		if (dead && grounded)
		{
			Rect buttonRect = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
			if (GUI.Button(buttonRect, "Tap to restart!"))
			{
				Application.LoadLevel (Application.loadedLevelName);
				coins = 0;
				GeneratorScript.currentLevel = 0;
			};

		}
	}

	void AdjustFootstepsAndJetpackSound(bool jetpackActive)    
	{
		footstepsAudio.enabled = !dead && grounded;
		
		jetpackAudio.enabled =  !dead && !grounded;
		jetpackAudio.volume = jetpackActive ? 1.0f : 0.5f;        
	}
}
